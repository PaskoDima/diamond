<?php

namespace RoockLab\GuestToCustomer\Plugin;

class PluginBeforeView
{

    public function beforeGetOrderId(\Magento\Sales\Block\Adminhtml\Order\View $subject){

        if(!$subject->getOrder()->getCustomerId()){
            $orderId = $subject->getOrder()->getIncrementId();
            $subject->addButton(
                'mybutton',
                ['label' => __('Create Customer'), 'onclick' => 'setLocation(\'' . '/index.php/guesttocustomer?data=' .$orderId . ' \')'],
                -1
            );
        }
        return null;
    }

}