<?php

namespace RoockLab\GuestToCustomer\Controller\Index;

use Magento\Framework\App\Action\Context;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;


    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $custommergrop = 1;

        $orderId = $_GET['data'];
        $order = $this->_objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($orderId);
        $orderAddress = $order->getShippingAddress();


        $url = \Magento\Framework\App\ObjectManager::getInstance();

        $storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface');

//        $state = $this->_objectManager->get('\Magento\Framework\App\State');
//
//        $state->setAreaCode('frontend');

        // Customer Factory to Create Customer

        $customerFactory = $this->_objectManager->get('\Magento\Customer\Model\CustomerFactory');

        $websiteId = $storeManager->getWebsite()->getWebsiteId();

        $store = $storeManager->getStore();

        $storeId = $store->getStoreId();

        // Instantiate object (this is the most important part)

        $customer = $customerFactory->create();

        $customer->setWebsiteId($websiteId);

        $customer->setEmail($order->getCustomerEmail());

        $customer->setFirstname($orderAddress->getFirstname());

        $customer->setLastname($orderAddress->getLastname());

        $customer->setPassword("123456789");

        $customer->save();

        $customerId = $customer->getId();

        $addresss = $this->_objectManager->get('\Magento\Customer\Model\AddressFactory');

        $address = $addresss->create();

        $address->setCustomerId($customer->getId())
            ->setFirstname($orderAddress->getFirstname())
            ->setLastname($orderAddress->getLastname())
            ->setCountryId($orderAddress->getCountryId())
            ->setPostcode($orderAddress->getPostcode())
            ->setCity($orderAddress->getCity())
            ->setRegion($orderAddress->getRegion())
            ->setTelephone($orderAddress->getTelephone())
            ->setFax($orderAddress->getFax())
            ->setCompany($orderAddress->getCompany())
            ->setStreet($orderAddress->getStreet())
            ->setIsDefaultBilling('1')
            ->setIsDefaultShipping('1')
            ->setSaveInAddressBook('1');

        $address->save();

        $orderCollection = $this->_objectManager->get('Magento\Sales\Model\Order')->getCollection()->addFieldToFilter('customer_email', $order->getCustomerEmail());

        foreach ($orderCollection as $sorder) {

            $sorder->setCustomerId($customerId);
            $sorder->setCustomerFirstname($orderAddress->getFirstName());
            $sorder->setCustomerLastname($orderAddress->getLastName());
            $sorder->setCustomerIsGuest(0);
            $sorder->setCustomerGroupId($custommergrop);

            $sorder->save();
        }

//        $this->messegeManeger = $this->_objectManager->get('\Magento\Framework\Message\ManagerInterface'); // just on frontend
//        $this->messegeManeger->addSuccess(__("Customer Successfully Created")); //???
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setRefererUrl();
        return $resultRedirect;
    }
}