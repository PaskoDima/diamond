<?php
error_reporting(1);
ini_set('max_execution_time', 0);

use \Magento\Framework\App\Bootstrap;
require __DIR__ . '/app/bootstrap.php';
$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$url = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface');
$mediaurl= $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
$state = $objectManager->get('\Magento\Framework\App\State');
$state->setAreaCode('frontend');

$setup = $objectManager->create('Magento\Framework\Setup\ModuleDataSetupInterface');
$eavSetupFactory = $objectManager->create('Magento\Eav\Setup\EavSetupFactory');
$eavConfig = $objectManager->create('\Magento\Eav\Model\Config');

//$eavSetup = $eavSetupFactory->create(['setup' => $setup]);
//$entityType = $eavSetup->getEntityTypeId('catalog_product');

//$eavSetup->updateAttribute($entityType, 'shape', 'frontend_input','select', null);
//$eavSetup->updateAttribute($entityType, 'shape', 'swatch_input_type','visual', null);
//$eavSetup->updateAttribute($entityType, 'shape', 'update_product_preview_image',1, null);
//$eavSetup->updateAttribute($entityType, 'shape', 'use_product_image_for_swatch',0, null);
//$eavSetup->updateAttribute($entityType, 'shape', 'used_in_product_listing' , true, null);


//if (($handle = fopen("test.csv", "r")) !== FALSE) {
//    while (($data = fgetcsv($handle)) !== FALSE) {
//        $attr = explode(',', substr($data[46], 0, -1)); //additional attributes without ',' and delete last symbol
//        $singleAttr = [];
//        foreach ($attr as $item) {
//            $explodeStr = explode('=',$item);
//            $singleAttr[$explodeStr[0]] = $explodeStr[1]; // additional attributes without '='
////            $eavSetup->updateAttribute($entityType, $explodeStr[0], 'frontend_input','select', null);
////            $eavSetup->updateAttribute($entityType, $explodeStr[0], 'used_in_product_listing' , true, null);
//        }
//    }
//    fclose($handle);
//}
//var_dump($singleAttr);



$attributesMulty = ['polish','symmetry','fluorescence','certificate'];
$attributesDropDown = ['sellername','rapnetaccountid','namecode','carat','depth','table'];//City,State,Country
$carat = ['symmetry','fluorescence'];
$yesNo =['blackinclusion', 'centerinclusion','milky'];//or select?
$attributesSwatchas = ['shape'];
$priceAttr = ['PricePerCarat','PricePercentage'];
$textArea = ['membercomments', 'certcomments'];
//simple input : Ratio,CertificateNumber,StockNumber,Treatment,Availability,Measurements,Girdle,Culet,Pavilion,CertificateURL,ImageURL,DiamondID

//}

foreach ($yesNo as $item) {
    $eavSetup = $eavSetupFactory->create(['setup' => $setup]);
    $eavConfig->clear();
    $attribute = $eavConfig->getAttribute('catalog_product', $item);
    if (!$attribute) {
        return;
    }
    $options = [];
    $optionCollection = [];
    $attributeId = $attribute->getId();
    if ($attributeId) {
        if (empty($optionCollection[$attributeId])) {
            $optionCollection[$attributeId] = $objectManager->create('\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory')->create() //\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory
            ->setAttributeFilter($attributeId)
                ->setPositionOrder('asc', true)
                ->load();
        }
        foreach ($optionCollection[$attributeId] as $option) {
            $options[$option->getId()] = $option->getValue();
        }
    }
    $attributeData['option'] = $options;
    $attributeData['frontend_input'] = 'select';//textarea
//    $attributeData['frontend_input'] = 'price';
//    $attributeData['backend_type'] = 'decimal';
    $attributeData['update_product_preview_image'] = 1;
    $attributeData['use_product_image_for_swatch'] = 0;
//    $attributeData['optionvisual'] = $this->getOptionSwatch($attributeData);
//    $attributeData['defaultvisual'] = $this->getOptionDefaultVisual($attributeData);
//    $attributeData['swatchvisual'] = $this->getOptionSwatchVisual($attributeData);
    $attribute->addData($attributeData);
    $attribute->save();
}